package exceptions;

import annotations.ThisCodeSmells;

@ThisCodeSmells(reviewer = "Sergey Ilin")
public final class WrongSortingParametersException extends VehicleException {
    @ThisCodeSmells(reviewer = "Sergey Ilin")
    public WrongSortingParametersException(String message) {
        super(message);
    }
}
