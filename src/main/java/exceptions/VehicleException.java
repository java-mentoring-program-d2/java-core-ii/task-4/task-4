package exceptions;

public abstract class VehicleException extends RuntimeException {
    public VehicleException(String message) {
        super(message);
    }
}
